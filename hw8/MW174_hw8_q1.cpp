#include <iostream>
using namespace std;

int minInArray(int arr[], int arrSize);
//input an array of integers and its logical size, returns the minimum value in the array

void printValIndexes(int arr[], int arrSize, int val);
//input an array of integers, its logical size, and some element of the array
//returns the indexes of the specified element in the array

int main() {
    int input_n, input_arr[20], min_arr_val;

    cout << "Please enter 20 integers separated by a space:\n";
    for (int i = 0; i < 20; i++) {
        cin >> input_n;
        input_arr[i] = input_n;
    }

    min_arr_val = minInArray(input_arr, 20);

    cout << "The minimum value is " << min_arr_val << ", and it is located in the following indices: ";
    
    printValIndexes(input_arr, 20, min_arr_val);
    cout << endl;

    return 0;
}

int minInArray(int arr[], int arrSize) {
    int min = arr[0];
    for (int i = 0; i < arrSize; i++) {
        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return min;
}

void printValIndexes(int arr[], int arrSize, int val) {
    for (int i = 0; i < arrSize; i++) {
        if (arr[i] == val) {
            cout << i << " ";
        }
    }
}
