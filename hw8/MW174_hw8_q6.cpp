#include <string>
#include <iostream>
using namespace std;

bool checkIfWordIsNumber(string str, int start_index);
//checks if word at starting index within a string is a number

int main() {
    string input_line;
    int str_len;
    bool wordisdigit;

    cout << "Please enter a line of text:\n";
    getline(cin, input_line);
    str_len = input_line.length();

    wordisdigit = checkIfWordIsNumber(input_line, 0);

    for (int i = 0; i < str_len; i++) {
        if (input_line[i] == ' ') {
            int j = i + 1;
            wordisdigit = checkIfWordIsNumber(input_line, j);
        } else if (((input_line[i] >= '0') && (input_line[i] <= '9')) && (wordisdigit)) {
            input_line[i] = 'x';
        }
    }

    cout << input_line;

    return 0;
}

bool checkIfWordIsNumber(string str, int start_index) {
    int j = start_index;
    while (str[j] != ' ') {
        if (!((str[j] >= '0') && (str[j] <= '9')))
        {
            return false;
        }
        else {
            j++;
        }
    }
    return true;
}