#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

void random_123_arr(int arr[], int arrSize);
//generates an array with random integers of either 1, 2, or 3

bool pin_match(int input_pin_arr[], int rand_arr[], const int pin_arr[], int pin_length);
//checks whether an input_pin is the correct pin, returns truth value

int main() {

    const int PIN[5] = {1, 2, 3, 4, 5};
    int rand_arr[10], input_pin, input_pin_arr[5];

    cout << "Please enter your PIN according to the following mapping:\n";
    cout << "PIN: 0 1 2 3 4 5 6 7 8 9\n";
    cout << "NUM: ";
    random_123_arr(rand_arr, 10);
    for (int i = 0; i < 10; i++) {
        cout << rand_arr[i] << " ";
    }
    cout << endl;

    cin >> input_pin;
    for (int j = 4; j >= 0; j--) {
        input_pin_arr[j] = input_pin % 10;
        input_pin /= 10;
    }

    if (pin_match(input_pin_arr, rand_arr, PIN, 5)) {
        cout << "Your PIN is correct";
    }
    else {
        cout << "Your PIN is not correct";
    }

    return 0;
}

void random_123_arr(int arr[], int arrSize) {
    int rand_num;
    srand(time(0));
    for (int i = 0; i < arrSize; i++) {
        rand_num = (rand() % 3) + 1;
        arr[i] = rand_num;
    }
}

bool pin_match(int input_pin_arr[], int rand_arr[], const int pin_arr[], int pin_length) {
    bool pinMatch;
    for (int k = 0; k < pin_length; k++) {
        if (input_pin_arr[k] != rand_arr[pin_arr[k]]) {
            pinMatch = false;
            return pinMatch;
        }
        else {
            pinMatch = true;
        }
    }
    return pinMatch;
}