#include <iostream>
#include <string>
using namespace std;

bool isPalindrome(string str);
//returns truth value of whether an input string is a palindrome (true) or not (false)

int main() {

    string word;

    cout << "Please enter a word: ";
    cin >> word;

    if (isPalindrome(word)) {
        cout << word << " is a palindrome";
    }
    else {
        cout << word << " is NOT a palindrome";
    }

    return 0;
}

bool isPalindrome(string str) {
    int wordLength = str.length();
    bool palindrome;

    for (int letter = 0; letter < wordLength; letter++) {
        if (str[letter] != str[((wordLength - 1) - letter)]) {
            palindrome = false;
        }
        else {
            palindrome = true;
        }
    }
    return palindrome;
}