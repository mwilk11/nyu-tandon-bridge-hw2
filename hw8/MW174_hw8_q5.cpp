#include <iostream>
#include <string>
using namespace std;

int main() {
    string first_name, middle_name, last_name;

    cout << "Please input your first name, middle name, and last name all separated by a space\n";
    cin >> first_name >> middle_name >> last_name;

    cout << last_name << ", " << first_name << " " << middle_name[0] << "." << endl;

    return 0;
}