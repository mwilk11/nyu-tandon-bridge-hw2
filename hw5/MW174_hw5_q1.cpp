#include <iostream>
using namespace std;

int main() {

    int input_n, rows, columns;
    char tab = '\t';

    cout<<"Please enter a positive integer:\n";
    cin>>input_n;

    for (rows = 1; rows <= input_n; rows++){
        for (columns = 1; columns <= input_n; columns++){
            cout<<rows * columns;
            cout<<tab;
        }
        cout<<endl;
    }

    return 0;
}