#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

int main() {

    int rand_num, guess_num, guesses, range_lo, range_hi;
    bool game_over;
    const int TOTAL_GUESSES = 5;

    srand(time(0));
    rand_num = (rand() % 100) + 1;

    cout<<"I thought of a number between 1 and 100! Try to guess it.\n";

    game_over = false;
    guesses = 0;
    range_lo = 1, range_hi = 100;

    while (game_over == false) {
        cout<<"Range: ["<<range_lo<<", "<<range_hi<<"], Number of guesses left: "<<TOTAL_GUESSES - guesses<<endl;
        cout<<"Your guess: ";
        cin>>guess_num;
        guesses += 1;

        if (guess_num == rand_num) {
            cout<<"Congrats! You guessed my number in "<<guesses<<" guesses.\n";
            game_over = true;
        }
        else {
            if ((guess_num != rand_num) && (guesses == TOTAL_GUESSES)){
                cout<<"Out of guesses! My number is "<<rand_num<<endl;
                game_over = true;
            }
            else if (guess_num > rand_num) {
                cout<<"Wrong! My number is smaller.\n"<<endl;
                if (range_hi > guess_num) {
                    range_hi = guess_num - 1;
                }
            }
            else {
                cout<<"Wrong! My number is bigger.\n"<<endl;
                if (range_lo < guess_num) {
                    range_lo = guess_num + 1;
                }
            }
        }
    }

    return 0;
}