#include <iostream>
using namespace std;

int printMonthCalender(int numOfDays, int startingDay);
// Prints a formatted monthly calendar of that month
// Returns a number 1-7 that represents the day in the week of the last day in that month

bool isLeapYear(int year);
// Takes in a year as input and returns true if the year is a leap year, returns false otherwise

void printYearCalender(int year, int startingDay);
// Uses printMonthCalendar and isLeapYear to print a formatted yearly calendar of that year

int main() {

    int year, starting_day;

    cout << "Please input a year: ";
    cin >> year;
    cout << "Please input the number of the day of the week (1-7) for January 1st of that year: ";
    cin >> starting_day;
    cout << endl;

    printYearCalender(year, starting_day);

    return 0;
}

int printMonthCalender(int numOfDays, int startingDay) {
    cout << "Mon\tTue\tWed\tThr\tFri\tSat\tSun\n";
    int week_counter = startingDay;
    for (int tab = 1; tab < startingDay; tab++) {
        cout << "\t";
    }
    for (int day = 1; day <= numOfDays; day++) {
        cout << day << "\t";
        week_counter += 1;
        if (week_counter == 8) {
            week_counter = 1;
            if (day != numOfDays){
                cout << endl;
            }
        }
        if (day == numOfDays) {
            cout << endl;
        }
    }
    cout << endl;
    return (week_counter);
}

bool isLeapYear(int year) {
    return ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)));
}

void printYearCalender(int year, int startingDay) {
    int last_day;

    cout << "January " << year << endl;
    last_day = printMonthCalender(31, startingDay);

    cout << "February " << year << endl;
    if (isLeapYear(year)) {
        last_day = printMonthCalender(29, last_day);
    }
    else {
        last_day = printMonthCalender(28, last_day);
    }

    cout << "March " << year << endl;
    last_day = printMonthCalender(31, last_day);

    cout << "April " << year << endl;
    last_day = printMonthCalender(30, last_day);

    cout << "May " << year << endl;
    last_day = printMonthCalender(31, last_day);

    cout << "June " << year << endl;
    last_day = printMonthCalender(30, last_day);

    cout << "July " << year << endl;
    last_day = printMonthCalender(31, last_day);

    cout << "August " << year << endl;
    last_day = printMonthCalender(31, last_day);

    cout << "September " << year << endl;
    last_day = printMonthCalender(30, last_day);

    cout << "October " << year << endl;
    last_day = printMonthCalender(31, last_day);

    cout << "November " << year << endl;
    last_day = printMonthCalender(30, last_day);

    cout << "December " << year << endl;
    printMonthCalender(31, last_day);
}