#include <iostream>
#include <cmath>
using namespace std;

void analyzeDividors(int num, int& outCountDivs, int& outSumDivs);

bool isPerfect(int num);

int main() {

    int input_n, x, y, a, b;

    cout << "Please input an integer >=2: ";
    cin >> input_n;

    for (int i = 2; i <= input_n; i++) {
        if (isPerfect(i)) {
            cout << i << endl;
        }
    }

    for (int i = 2; i <= input_n; i++) {
        a = 0, b = 0;
        analyzeDividors(i, a, b);
        for (int j = 3; j <= input_n; j++) {
            x = 0, y = 0;
            analyzeDividors(j, x, y);
            if ((i == y) && (j == b) && (i != j)) {
                cout << i << "," << j << endl;
            }
        }
    }

    return 0;
}

void analyzeDividors(int num, int& outCountDivs, int& outSumDivs) {
    for (int i = 1; i <= (sqrt(num)); i++){
        if ((num % i) == 0){
            outSumDivs = outSumDivs + i;
            outCountDivs += 1;
        }
    }
    for (int i = (sqrt(num)); i > 1; i--) {
        if (((num % i) == 0) && (i != sqrt(num))) {
            outSumDivs += (num/i);
            outCountDivs += 1;
        }
    }
}

bool isPerfect(int num) {
    int a = 0, b = 0;
    analyzeDividors(num, a, b);
    return (num == b);
}