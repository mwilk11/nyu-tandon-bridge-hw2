#include <iostream>
using namespace std;

int fib(int n);
//returns the nth fibonacci number

int main() {
    int num, fib_num;

    cout<<"Please enter a positive integer: ";
    cin>>num;

    fib_num = fib(num);
    cout<<fib_num;

    return 0;
}

int fib(int n) {
    if (n == 1) {
        return 1;
    }
    else if (n == 2) {
        return 1;
    }
    else {
        return fib(n-1)+fib(n-2);
    }
}