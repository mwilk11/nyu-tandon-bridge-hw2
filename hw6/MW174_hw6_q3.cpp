#include <iostream>
using namespace std;

int factorial(int n);
//returns the nth factorial number

double eApprox(int n);
//returns e to the nth infinite sum approximation

int main() {

    cout.precision(30);
    
    for (int n = 1; n <= 15; n++) {
        cout<<"n = "<<n<<'\t'<<eApprox(n)<<endl; }

    return 0;
}

double eApprox(int n) {
    double e_inf_sum = 1;
    for (int i = 1; i <= n; i++) {
        e_inf_sum += (1.0 / factorial(i));
    }
    return e_inf_sum;
}

int factorial(int n) {
    int fact_n = 1;
    for (int i = 2; i <= n; i++) {
        fact_n *= i;
    }
    return fact_n;
}
