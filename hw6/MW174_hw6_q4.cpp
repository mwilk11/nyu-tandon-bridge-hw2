#include <iostream>
#include <cmath>
using namespace std;

void printDivisors(int num);
//Takes positive integer input, and prints all of its divisors in an ascending order, separated by a space

int main() {
    int input_n;

    cout<<"Please enter a positive integer >= 2: ";
    cin>>input_n;

    printDivisors(input_n);

    return 0;
}

void printDivisors(int num){
    for (int i = 1; i <= (sqrt(num)); i++){
        if ((num % i) == 0){
            cout<<i<<" ";
        }
    }
    for (int i = (sqrt(num)); i >= 1; i--) {
        if (((num % i) == 0) && (i != sqrt(num))) {
            cout<<(num/i)<<" ";
        }
    }
}