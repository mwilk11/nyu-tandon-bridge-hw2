#include <iostream>
using namespace std;

void printShiftedTriangle(int n, int m, char symbol);
//prints an n-line triangle, filled with symbol characters, shifted m spaces from the left margin

void printPineTree(int n, char symbol);
//prints a sequence of n triangles of increasing sizes (the smallest triangle is a 2-line triangle)
// which form the shape of a pine tree. The triangles are filled with the symbol character


int main() {

    int input_n;
    char input_symbol;

    cout<<"Please input the number of triangle tiers you would like in your tree: ";
    cin>>input_n;
    cout<<"Please input a character to fill the tree: ";
    cin>>input_symbol;

    printPineTree(input_n, input_symbol);

    return 0;
}

void printShiftedTriangle(int n, int m, char symbol) {

    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= m; j++) {
            cout<<" ";
        }
        for (int k = 1; k <= n-i; k++) {
            cout<<" ";
        }
        for (int l = 3; l <= ((2*i)+1); l++) {
            cout<<symbol;
        }
        cout<<endl;
    }
}

void printPineTree(int n, char symbol) {
    int space_counter = n - 1;

    for (int i = 2; i <= (n + 1); i++) {
        printShiftedTriangle(i, space_counter, symbol);
        space_counter -= 1;
    }
}