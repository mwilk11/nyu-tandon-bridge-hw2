#include <iostream>
using namespace std;

//section a
void printTriangle(int n);

//section b
void printOpositeTriangles(int n);

//section c
void printRuler(int n);

int main() {

    cout << "section a" << endl;
    printTriangle(4);
    cout << endl;

    cout << "section b" << endl;
    printOpositeTriangles(4);
    cout << endl;

    cout << "section c" << endl;
    printRuler(4);
    cout << endl;

    return 0;
}

void printTriangle(int n) {
    if (n == 1) {
        cout << '*' << endl;
    }
    else {
        printTriangle((n-1));
        for (int i = 0; i < n; i++) {
            cout << '*';
        }
        cout << endl;
    }
}

void printOpositeTriangles(int n) {
    for (int i = 0; i < n; i++) {
        cout << '*';
    }
    cout << endl;
    if (n > 1) {
        printOpositeTriangles((n - 1));
    }
    for (int i = 0; i < n; i++) {
        cout << '*';
    }
    cout << endl;
}

void printRuler(int n) {
    if (n == 1) {
        cout << '-' << endl;
    }
    else {
        printRuler((n-1));
        for (int i = 0; i < n; i++) {
            cout << '-';
        }
        cout << endl;
        printRuler((n-1));
    }
}