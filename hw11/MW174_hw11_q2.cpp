#include <iostream>
using namespace std;

//section a
int sumOfSquares(int arr[], int arrSize);

//section b
bool isSorted(int arr[], int arrSize);

int main() {

    int test_arr[3] = {-1, 0, 7};
    int arr_size = 3;

    cout << "section a" << endl;
    cout << sumOfSquares(test_arr, arr_size);
    cout << endl;

    cout << "section b" << endl;
    cout << isSorted(test_arr, arr_size);
    cout << endl;

    return 0;
}

int sumOfSquares(int arr[], int arrSize) {
    if (arrSize == 0) {
        return 0;
    }
    return sumOfSquares(arr, (arrSize - 1)) + (arr[(arrSize - 1)] * arr[(arrSize - 1)]);
}

bool isSorted(int arr[], int arrSize) {
    //if (arrSize <= 1)
        //return false;
    if (arrSize == 2) {
        return arr[(arrSize - 1)] >= arr[(arrSize - 2)];
    }
    else {
        return isSorted(arr, (arrSize - 1)) && (arr[(arrSize - 1)] >= arr[(arrSize - 2)]);
    }
}