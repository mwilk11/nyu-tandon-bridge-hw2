#include <iostream>
using namespace std;

int jumpItMin(int arr[], int arrSize);

int main() {

    int arr[6] = {0, 3, 80, 6, 57, 10};
    cout << jumpItMin(arr, 6);

    return 0;
}

int jumpItMin(int arr[], int arrSize) {
    int min_sum = 0;
    if (arrSize == 1 || arrSize == 0) {
        return 0;
    }
    else {
        if (arr[(arrSize - 2)] < arr[(arrSize - 1)]) {
            min_sum = jumpItMin(arr, (arrSize - 2)) + arr[(arrSize - 2)];
        }
        else {
            min_sum = jumpItMin(arr, (arrSize - 1)) + arr[(arrSize - 1)];
        }
    }
    return min_sum;
}