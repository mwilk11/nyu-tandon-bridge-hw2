#include <iostream>
using namespace std;

void oddsKeepEvensFlip(int arr[], int arrSize);
//When called, reorders elements of arr so that:
//All the odd numbers come before all the even numbers
//The odd numbers will keep their original relative order
//The even numbers will be placed in a reversed order (relative to their original order).

int main() {

    int arr[6] = {5, 2, 11, 7, 6, 4};

    oddsKeepEvensFlip(arr, 6);

    for (int i = 0; i < 6; i++) {
        cout << arr[i] << " ";
    }

    return 0;
}

void oddsKeepEvensFlip(int arr[], int arrSize) {
    int arr2[arrSize];
    int odd_idx_count = 0;
    int even_idx_count = (arrSize - 1);
    for (int i = 0; i < arrSize; i++) {
        if (((arr[i] - 1) % 2) == 0) {
            arr2[odd_idx_count] = arr[i];
            odd_idx_count += 1;
        }
        if ((arr[i] % 2) == 0) {
            arr2[even_idx_count] = arr[i];
            even_idx_count -= 1;
        }
    }
    for (int j = 0; j < arrSize; j++) {
        arr[j] = arr2[j];
    }
}