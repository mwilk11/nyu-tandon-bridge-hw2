#include <iostream>
#include <string>
using namespace std;

int wordCount(string str);
//takes an input string and returns the word count of the words in the string

int main() {
    string text_line;
    int str_len;
    char letter;
    const int LETTERS_IN_ALPHABET = 26;
    int letter_count[LETTERS_IN_ALPHABET] = {0};

    cout << "Please enter a line of text:\n";
    getline(cin, text_line);
    str_len = text_line.length();

    cout << wordCount(text_line) << "\twords" << endl;

    for (int i = 0; i < str_len; i++) {
        letter = (char)tolower(text_line[i]);
        if ((letter >= 'a') && (letter <= 'z')) {
            letter_count[(letter - 'a')] += 1;
        }
    }

    for (int j = 0; j < LETTERS_IN_ALPHABET; j++) {
        if (letter_count[j] != 0) {
            cout <<letter_count[j] << "\t" << (char)(j + 'a') << endl;
        }
    }

    return 0;
}

int wordCount(string str) {
    int count = 0;
    for (char i : str) {
        if ((i == ' ') || (i == ',') || (i == '.') || (i == '\n')) {
            count += 1;
        }
    }
    if (str[(str.length() - 1)] != '.') {
        count += 1;
    }
    return count;
}