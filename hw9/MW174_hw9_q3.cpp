#include <iostream>
using namespace std;

int* getPosNums1(int* arr, int arrSize, int& outPosArrSize);
int* getPosNums2(int* arr, int arrSize, int* outPosArrSizePtr);
void getPosNums3(int* arr, int arrSize, int*& outPosArr, int& outPosArrSize);
void getPosNums4(int* arr, int arrSize, int** outPosArrPtr, int* outPosArrSizePtr);

int main() {

    int* pointer;
    int* result_pointer;
    int* len_pointer = new int;
    int* result_pos_arr = new int;

    int test_arr1[10] = {-3, -2, -1, 0, 1, 2, 3, 4, 5, 6};
    int arr_size1 = 10;

    int test_arr2[2] = {-2, 2};
    int arr_size2 = 2;

    int test_arr3[6] = {0, 0, 0, 3, -56, -100};
    int arr_size3 = 6;

    int test_arr4[10] = {-3, -1, -3, 0, 6, -4, 5, -9, 10, 11};
    int arr_size4 = 10;

    pointer = test_arr1;

    cout << "The contents of array 1 are: ";
    for (int i = 0; i < arr_size1; i++) {
        cout << pointer[i] << " ";
    }
    cout << endl << "The size of array 1 is: " << arr_size1 << endl;
    result_pointer = getPosNums1(pointer, arr_size1, arr_size1);
    cout << "The size of the new array after function 1 is: " << arr_size1 << endl;
    cout << "After function pointer points to: " << *result_pointer << endl;
    cout << "The contents of the new array are: ";
    for (int i = 0; i < arr_size1; i++) {
        cout << result_pointer[i] << " ";
    }
    cout << endl << endl;


    pointer = test_arr2;
    *len_pointer = arr_size2;

    cout << "The contents of array 2 are: ";
    for (int i = 0; i < arr_size2; i++) {
        cout << pointer[i] << " ";
    }
    cout << endl << "The size of array 2 is: " << *len_pointer << endl;
    result_pointer = getPosNums2(pointer, arr_size2, len_pointer);
    cout << "The size of the new array after function 2 is: " << *len_pointer << endl;
    cout << "After function pointer points to: " << *result_pointer << endl;
    cout << "The contents of the new array are: ";
    for (int i = 0; i < *len_pointer; i++) {
        cout << result_pointer[i] << " ";
    }
    cout << endl << endl;


    pointer = test_arr3;

    cout << "The contents of array 3 are: ";
    for (int i = 0; i < arr_size3; i++) {
        cout << pointer[i] << " ";
    }
    cout << endl << "The size of array 3 is: " << arr_size3 << endl;
    getPosNums3(pointer, arr_size3, result_pos_arr, arr_size3);
    cout << "The size of the new array after function 3 is: " << arr_size3 << endl;
    cout << "After function pointer points to: " << *result_pos_arr << endl;
    cout << "The contents of the new array are: ";
    for (int i = 0; i < arr_size3; i++) {
        cout << result_pos_arr[i] << " ";
    }

    cout << endl << endl;


    pointer = test_arr4;
    *len_pointer = arr_size4;

    cout << "The contents of array 4 are: ";
    for (int i = 0; i < arr_size4; i++) {
        cout << pointer[i] << " ";
    }
    cout << endl << "The size of array 4 is: " << *len_pointer << endl;
    getPosNums4(pointer, arr_size4, &result_pos_arr, len_pointer);
    cout << "The size of the new array after function 4 is: " << *len_pointer << endl;
    cout << "After function pointer points to: " << *result_pos_arr << endl;
    cout << "The contents of the new array are: ";
    for (int i = 0; i < *len_pointer; i++) {
        cout << result_pos_arr[i] << " ";
    }

    return 0;
}

int* getPosNums1(int* arr, int arrSize, int& outPosArrSize) {
    int* arr2;
    arr2 = new int[arrSize];
    int pos_idx_counter = 0;
    for (int i = 0; i < arrSize; i++) {
        if (arr[i] <= 0) {
            outPosArrSize -= 1;
        }
        else {
            arr2[pos_idx_counter] = arr[i];
            pos_idx_counter += 1;
        }
    }
    return arr2;
}

int* getPosNums2(int* arr, int arrSize, int* outPosArrSizePtr) {
    int* arr2;
    arr2 = new int[arrSize];
    int pos_idx_counter = 0;
    for (int i = 0; i < arrSize; i++) {
        if (arr[i] <= 0) {
            *outPosArrSizePtr -= 1;
        }
        else {
            arr2[pos_idx_counter] = arr[i];
            pos_idx_counter += 1;
        }
    }
    return arr2;
}

void getPosNums3(int* arr, int arrSize, int*& outPosArr, int& outPosArrSize) {
    int* arr2;
    arr2 = new int[arrSize];
    int pos_idx_counter = 0;
    for (int i = 0; i < arrSize; i++) {
        if (arr[i] <= 0) {
            outPosArrSize -= 1;
        }
        else {
            arr2[pos_idx_counter] = arr[i];
            pos_idx_counter += 1;
        }
    }
    outPosArr = arr2;
}

void getPosNums4(int* arr, int arrSize, int** outPosArrPtr, int* outPosArrSizePtr) {
    int* arr2;
    arr2 = new int[arrSize];
    int pos_idx_counter = 0;
    for (int i = 0; i < arrSize; i++) {
        if (arr[i] <= 0) {
            *outPosArrSizePtr -= 1;
        }
        else {
            arr2[pos_idx_counter] = arr[i];
            pos_idx_counter += 1;
        }
    }
    *outPosArrPtr = arr2;
}
