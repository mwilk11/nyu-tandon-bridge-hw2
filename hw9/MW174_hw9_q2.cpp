#include <iostream>
#include <string>
using namespace std;

const int LETTERS_IN_ALPHABET = 26;

void fillAlphabetCountArr(string str, int letter_count_arr[LETTERS_IN_ALPHABET]);
//takes input of a string and an array, fills the array with counts for each letter in alphabetical order

bool isAnagram(int letter_count1[LETTERS_IN_ALPHABET], int letter_count2[LETTERS_IN_ALPHABET]);
//takes input of two array each containing counts of all the letters from some text, returns whether they are anagrams

int main() {
    
    string text_line1, text_line2;
    int letter_count1[LETTERS_IN_ALPHABET] = {0};
    int letter_count2[LETTERS_IN_ALPHABET] = {0};

    cout << "Please input a string of text:\n";
    getline(cin, text_line1);
    fillAlphabetCountArr(text_line1, letter_count1);

    cout << "Please input another string of text:\n";
    getline(cin, text_line2);
    fillAlphabetCountArr(text_line2, letter_count2);

    if (isAnagram(letter_count1, letter_count2)) {
        cout << "YES the two inputs are anagrams" << endl;
    }
    else {
        cout << "NO the two inputs are NOT anagrams" << endl;
    }

    return 0;
}

void fillAlphabetCountArr(string str, int letter_count_arr[LETTERS_IN_ALPHABET]) {
    char letter;
    for (int i = 0; i < str.length(); i++) {
        letter = (char)tolower(str[i]);
        if ((letter >= 'a') && (letter <= 'z')) {
            letter_count_arr[(letter - 'a')] += 1;
        }
    }
}

bool isAnagram(int letter_count1[LETTERS_IN_ALPHABET], int letter_count2[LETTERS_IN_ALPHABET]) {
    for (int k = 0; k < LETTERS_IN_ALPHABET; k++) {
        if (letter_count1[k] != letter_count2[k]) {
            return false;
        }
    }
    return true;
}