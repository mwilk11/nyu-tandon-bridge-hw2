#include <iostream>
#include <vector>
using namespace std;

vector<int> minMax(vector<int> v, int start, int end) {
    vector<int> min_max{0,0};
    vector<int> left{0,0};
    vector<int> right{0,0};

    if (start == end) {
        min_max[0] = v[start];
        min_max[1] = v[start];
        return min_max;
    }
    if (end == start + 1) {
        if (v[start] < v[end]) {
            min_max[0] = v[start];
            min_max[1] = v[end];
        }
        else {
            min_max[0] = v[end];
            min_max[1] = v[start];
        }
        return min_max;
    }
    int mid = (start + end) / 2;
    left = minMax(v, start, mid);
    right = minMax(v, mid + 1, end);

    if (left[0] < right[0]) {
        min_max[0] = left[0];
    }
    else {
        min_max[0] = right[0];
    }
    if (left[1] > right[1]) {
        min_max[1] = left[1];
    }
    else {
        min_max[1] = right[1];
    }
    return min_max;
}

int main() {

    //TEST
    vector<int> vec2;
    vector<int> vec1{97, 1700, 53, 110, 47, 11, 0, 920, 65, 43, 0, -1, -322, -57};
    vec2 = minMax(vec1, 0, vec1.size() - 1);

    cout << "min: " << vec2[0] << endl;
    cout << "max: " << vec2[1] << endl;

    return 0;
}