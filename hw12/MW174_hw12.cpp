/* clang++ --std=c++11 --stdlib=libc++ -o <exec filename> main.cpp */

#include <iostream>
#include <cstdlib>
#include <cctype>
using namespace std;

int digit_to_int(char c);

class Money {
public:
    Money(long dollars, int cents);
    //initializes Money object to be amount in dollars and cents
    Money(long dollars);
    //initializes Money object so its value is $ dollars.00
    Money();
    //initializes Money object so its value is $0.00
    friend Money operator +(const Money& amount1, const Money& amount2);
    //returns sum of two const Money objects
    friend Money operator -(const Money& amount, const Money& amount2);
    //returns a negative Money object
    friend bool operator <(const Money& amount1, const Money& amount2);
    //returns true if amount1 is less than amount 2; otherwise returns false
    double get_value();
    //returns amount of money
    friend istream& operator >>(istream& ins, Money& amount);
    //overloads >> to input values of Money object
    friend ostream& operator <<(ostream& outs, const Money& amount);
    //overloads << to output values of Money object
private:
    long all_cents;
};

class Check {
private:
    int check_number;
    Money check_amount_value;
    bool cashed;
public:
    Check(int check_num, double check_amt, bool was_cashed);
    //initializes Check object to have check_number, check amount and whether it was cashed
    Check(double check_amt);
    //initialies Check object to have input value;
    Check();
    //initialies Check object to have 0.00 value;
    Money set_amount(double amount);
    //mutator for the check_amount_value
    Money get_amount();
    //accessor for the check_amount_value
    int set_check_number(int number);
    //mutator for setting the check number
    int get_check_number();
    //accessor for getting the check number
    bool set_cashed(bool was_cashed);
    //mutator for setting the cashed boolean variable
    bool get_cashed();
    //accessor for getting the cashed boolean variable
};



int main() {
    Money bank_balance;
    Money withdraw_amount;
    double check_amount;
    Money check_value;
    bool bank_cash;
    int check_num;
    Check pay_check;

    cout << "Please enter your starting balance in the form $x.xx, if no balance, enter $0.00\n";
    cin >> bank_balance;
    cout << "Please enter a withdrawal amount in the form $x.xx\n";
    cin >> withdraw_amount;

    bank_balance = bank_balance - withdraw_amount;
    cout << bank_balance << endl;

    cout << "Please enter a check number\n" << endl;
    cin >> check_num;
    cout << "Please enter a check amount\n" << endl;
    cin >> check_amount;
    cout << "Please enter if the checked was cashed 1 for yes 0 for no\n" << endl;
    cin >> bank_cash;
    pay_check = Check(check_num, check_amount, bank_cash);
    check_value = pay_check.get_amount();
    cout << check_value << endl;

    return 0;
}

Money::Money(long dollars, int cents) {
    if (dollars * cents < 0) {
        cout << "improper values for dollars and cents.\n";
        exit(1);
    }
    all_cents = dollars * 100 + cents;
}

Money::Money(long dollars): all_cents(dollars * 100) {}

Money::Money(): all_cents(0) {}

double Money::get_value() {
    return (all_cents * 0.01);
}

Money operator +(const Money& amount1, const Money& amount2) {
    Money temp_var;
    temp_var.all_cents = amount1.all_cents + amount2.all_cents;
    return temp_var;
}

Money operator -(const Money& amount1, const Money& amount2) {
    Money temp_var;
    temp_var.all_cents = amount1.all_cents - amount2.all_cents;
    return temp_var;
}

bool operator <(const Money& amount1, const Money& amount2) {
    return (amount1.all_cents < amount2.all_cents);
}

int digit_to_int(char c) {
    return (static_cast<int>(c) - static_cast<int>('0'));
}

istream& operator >>(istream& ins, Money& amount) {
    char first_char, money_sign, decimal_point, digit1, digit2;
    int dollars;
    int cents;
    bool negative;

    ins >> first_char;
    if (first_char == '-') {
        negative = true;
        ins >> money_sign;
    }
    else {
        negative = false;
        money_sign = first_char;
    }
    ins >> dollars >> decimal_point >> digit1 >> digit2;

    if (money_sign != '$' || decimal_point != '.') {
        cout << "Money not input correctly\n";
        exit(1);
    }

    cents = ((digit_to_int(digit1) * 10) + (digit_to_int(digit2)));

    amount.all_cents = (dollars * 100) + cents;
    if (negative) {
        amount.all_cents *= -1;
    }

    return ins;
}

ostream& operator <<(ostream& outs, const Money& amount) {
    long positive_cents, dollars, cents;
    positive_cents = labs(amount.all_cents);
    dollars = positive_cents / 100;
    cents = positive_cents % 100;
    if (amount.all_cents < 0) {
        outs << "-$" << dollars << '.';
    }
    else {
        outs << '$' << dollars << '.';
    }
    if (cents < 10) {
        outs << '0';
    }
    outs << cents;
    return outs;
}

Check::Check(int check_num, double check_amt, bool was_cashed) {
    int dollars, cents;
    check_number = check_num;
    dollars = (int)check_amt;
    cents = (check_amt - dollars) * 100;
    check_amount_value = Money(dollars, cents);
    cashed = was_cashed;
}

Check::Check(double check_amt): check_amount_value(check_amt) {}

Check::Check(): check_amount_value(0) {}

Money Check::set_amount(double amount) {
    int dollars, cents;
    dollars = (int)amount;
    cents = (amount - dollars) * 100;
    check_amount_value = Money(dollars, cents);
}

Money Check::get_amount() {
    return check_amount_value;
}

int Check::set_check_number(int number) {
    check_number = number;
}

int Check::get_check_number() {
    return check_number;
}

bool Check::set_cashed(bool was_cashed) {
    cashed = was_cashed;
}

bool Check::get_cashed() {
    return cashed;
}


