#include <iostream>
#include <string>
using namespace std;

int main() {

    int input_num, temp_num;

    char one = 'I';
    char five = 'V';
    char ten = 'X';
    char fifty = 'L';
    char hundred = 'C';
    char five_hundred = 'D';
    char thousand = 'M';

    const int I = 1;
    const int V = 5;
    const int X = 10;
    const int L = 50;
    const int C = 100;
    const int D = 500;
    const int M = 1000;

    string roman_numeral_num = "";

    cout<<"Enter decimal number:\n";
    cin>>input_num;

    temp_num = input_num;

    while (temp_num > 0) {
        if (temp_num >= M){
            roman_numeral_num += thousand;
            temp_num -= M;
        }
        else if ((temp_num >= D) && (temp_num < M)) {
            roman_numeral_num += five_hundred;
            temp_num -= D;
        }
        else if ((temp_num >= C) && (temp_num < D)) {
            roman_numeral_num += hundred;
            temp_num -= C;
        }
        else if ((temp_num >= L) && (temp_num < C)) {
            roman_numeral_num += fifty;
            temp_num -= L;
        }
        else if ((temp_num >= X) && (temp_num < L)) {
            roman_numeral_num += ten;
            temp_num -= X;
        }
        else if ((temp_num >= V) && (temp_num < X)) {
            roman_numeral_num += five;
            temp_num -= V;
        }
        else if ((temp_num >= I) && (temp_num < V)) {
            roman_numeral_num += one;
            temp_num -= I;
        }
    }

    cout<<input_num<<" is "<<roman_numeral_num;

    return 0;
}