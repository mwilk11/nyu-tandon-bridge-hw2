#include <iostream>
using namespace std;

int main() {

    int input_n, n, n2, even_counter, digit_counter, digit;

    cout<<"Input a positive integer\n";
    cin>>input_n;

    for (n = 1; n <= input_n; n++) {

        n2 = n;
        even_counter = 0;
        digit_counter = 0;

        while (n2 > 0) {
            digit = n2 % 10;

            if ((digit > 0) && (digit % 2 == 0)) {
                even_counter += 1;
            }
            n2 /= 10;
            digit_counter += 1;

            if (digit == 0) {
                digit_counter -=1;
            }
        }
        if (even_counter > (digit_counter - even_counter)) {
            cout<<n<<endl;
        }
    }

    return 0;
}