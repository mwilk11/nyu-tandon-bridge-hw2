#include <iostream>
#include <cmath>
using namespace std;

int main() {

    int input_n, temp_n, counter, power_of_2;
    long int bin_n;

    cout<<"Enter decimal number:\n";
    cin>>input_n;

    temp_n = input_n;
    bin_n = 0;

    for (counter = 10; counter >= 0; counter -= 1){
        
        power_of_2 = pow(2, counter);

        if (power_of_2 <= temp_n){
            temp_n -= power_of_2;
            bin_n += pow(10, counter);
        }
    }

    cout<<"The binary representation of "<<input_n<<" is "<<bin_n<<endl;

    return 0;
}