#include <iostream>
using namespace std;

int main() {

    int input_n, line_counter, space_counter, star_counter;

    cout<<"Input a positive integer\n";
    cin>>input_n;

    for (line_counter = input_n; line_counter >= 1; line_counter -= 1) {
        for (space_counter = input_n; space_counter > line_counter; space_counter -= 1) {
            cout<<" ";
        }
        for (star_counter = 1; star_counter < (line_counter * 2); star_counter += 1) {
            cout<<"*";
        }
        cout<<endl;
    }

    for (line_counter = 1; line_counter <= input_n; line_counter += 1) {
        for (space_counter = input_n; space_counter > line_counter; space_counter -= 1) {
            cout<<" ";
        }
        for (star_counter = 1; star_counter < (line_counter * 2); star_counter += 1) {
            cout<<"*";
        }
        cout<<endl;
    }

    return 0;
}