#include <iostream>
#include <cmath>
using namespace std;

int main() {

    int seq_len, counter1, counter2, input_a1, input_a2, inputs_product1, inputs_product2;
    double geom_mean1, geom_mean2;
    bool seq_cond;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(4);

    cout<<"section a\n";
    cout<<"Please enter the length of the sequence: ";
    cin>>seq_len;
    cout<<"Please enter your sequence:\n";

    inputs_product1 = 1;

    for (counter1 = 0;counter1 < seq_len; counter1++){
        cin>>input_a1;
        inputs_product1 *= input_a1;
    }

    geom_mean1 = pow(inputs_product1,(1.0/seq_len));
    cout<<"The geometric mean is: "<<geom_mean1<<endl;

    cout<<"section b\n";
    cout<<"Please enter a non-empty sequence of positive integers, each one in a separate line. ";
    cout<<"End your sequence by typing -1:\n";

    seq_cond = true;
    inputs_product2 = 1;
    counter2 = 0;

    while (seq_cond) {
        cin>>input_a2;
        if (input_a2 == -1) {
            seq_cond = false;
        }
        else {
            inputs_product2 *= input_a2;
            counter2 += 1;
        }
    }

    geom_mean2 = pow(inputs_product2,(1.0/counter2));
    cout<<"The geometric mean is: "<<geom_mean2;

    return 0;
}