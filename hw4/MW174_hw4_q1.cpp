#include <iostream>
using namespace std;

int main() {

    int even_num, input_n1, input_n2, counter1, counter2;

    even_num = 2;

    cout<<"section a\n";
    cout<<"Please enter a positive integer: ";
    cin>>input_n1;

    counter1 = 1;

    while (counter1 <= input_n1){
        cout<<even_num * counter1<<endl;
        counter1 += 1;
    }

    cout<<"section b\n";
    cout<<"Please enter a positive integer: ";
    cin>>input_n2;

    for (counter2 = 1; counter2 <= input_n2; counter2++){
        cout<<even_num * counter2<<endl;
    }

    return 0;
}