/*

SAMPLE PASCAL CODE:
/Users/Mikolaj/Desktop/pascal_sample.txt

BEGIN
   UserChoice := 'q';
   WHILE (UserChoice <> 'x') DO
      BEGIN
         ShowTheMenu;
         GetUserChoice;
         IF (UserChoice = 'a') OR
            (UserChoice = 'b') THEN
           BEGIN
              GetNumberToConvert;
              DoTheConversion;
              DisplayTheAnswer;
              Wait;
           END
      END
END

*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

void open_file(ifstream& input_file);
//opens a file

bool balance_check(ifstream& pascal_input);
//inputs ifstream of Pascal code by reference
//outputs whether the code is balanced in terms of begin/end, {}, (), and []
//returns true if balance, otherwise returns false

int main() {
    ifstream input_file;
    bool balanced;

    open_file(input_file);

    balanced = balance_check(input_file);

    if (balanced) {
        cout << "The program is balanced";
    }
    else {
        cout << "The program is NOT balanced";
    }

    return 0;
}

void open_file(ifstream& input_file) {
    string input_file_path;
    cout << "ENTER FILEPATH:\n";
    cin >> input_file_path;
    input_file.open(input_file_path);
    while (!input_file) {
        cout << "Error: file failed to open";
        cout << "ENTER FILEPATH:\n";
        cin >> input_file_path;
        input_file.clear();
        input_file.open(input_file_path);
    }
}

bool balance_check(ifstream& pascal_input) {
    string line;
    vector<char> symbols1;
    vector<char> symbols2;

    while (getline(pascal_input, line)) {

        if (line.find("begin") || line.find("BEGIN")) {
            symbols1.push_back('b');
        }
        if (line.find("end") || line.find("END")) {
            if (symbols1[(symbols1.size() - 1)] == 'b') {
                symbols1.pop_back();
            }
            else {
                symbols2.push_back('e');
            }
        }

        for (int i = 0; i < line.size(); i++) {
            if ((line[i] == '{') || (line[i] == '(') || (line[i] == '[')) {
                symbols1.push_back(line[i]);
            }
            if (line[i] == '}') {
                if (symbols1[(symbols1.size() - 1)] == '{') {
                    symbols1.pop_back();
            }
                else {
                    symbols2.push_back('}');
                }
            }
            if (line[i] == ')') {
                if (symbols1[(symbols1.size() - 1)] == '(') {
                    symbols1.pop_back();
                }
                else {
                    symbols2.push_back(')');
                }
            }
            if (line[i] == ']') {
                if (symbols1[(symbols1.size() - 1)] == '[') {
                    symbols1.pop_back();
                }
                else {
                    symbols2.push_back(']');
                }
            }
        }
    }
    return ((symbols1.size() == 0) && (symbols2.size() == 0));
}