#include <iostream>
#include <vector>
using namespace std;

template <class T>
class Queue {
    vector<T> queue_vector;
    int front_idx;
    int queue_size;
public:
    Queue();
    void push(const T& val);
    void pop();
    T peek();
    int size();
    void print_all();
};

template<class T>
Queue<T>::Queue() {
    vector<T> queue_vector;
    front_idx = 0;
    queue_size = 0;
}

template<class T>
void Queue<T>::push(const T& val) {
    queue_vector.push_back(val);
    queue_size += 1;
}

template<class T>
void Queue<T>::pop() {
    front_idx += 1;
    queue_size -=1;
    if (front_idx == queue_vector.size()) {
        queue_vector.clear();
        queue_size = 0;
        front_idx = 0;
    }
}

template<class T>
T Queue<T>::peek() {
    if (queue_vector.size() == 0) {
        return 0;
    }
    else {
        return queue_vector[front_idx];
    }
}

template<class T>
int Queue<T>::size() {
    return queue_size;
}

template<class T>
void Queue<T>::print_all() {
    if (queue_vector.size() == 0) {
        cout << "Queue is empty";
    }
    else {
        for (int i = front_idx; i < queue_vector.size(); i++) {
            cout << queue_vector[i] << ' ';
        }
    }
}

int main() {

    Queue<int> vector_queue;
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    vector_queue.push(1);
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    vector_queue.push(3);
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    vector_queue.push(5);
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    vector_queue.push(7);
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    vector_queue.push(9);
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    cout << vector_queue.peek() << " is at the front of the queue" << endl;
    vector_queue.pop();
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    cout << vector_queue.peek() << " is at the front of the queue" << endl;
    vector_queue.pop();
    cout << "Size of the queue is: " << vector_queue.size() << endl;
    cout << vector_queue.peek() << " is at the front of the queue" << endl;
    vector_queue.print_all();

    return 0;
}