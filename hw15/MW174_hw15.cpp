#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

template <class T>
class LList; //Pre-Definition to make Friend work in LListNode

template <class T>
class LListNode {
    T data;
    LListNode<T>* next;
    LListNode<T>* prev;
public:
    LListNode(const T& newdata = T(), LListNode<T>* newnext = nullptr, LListNode<T>* newprev = nullptr) :data(newdata), next(newnext), prev(newprev) {}
    friend class LList<T>;
};

template <class T>
class LList {
    LListNode<T>* head;
    LListNode<T>* tail;

public:
    LList();
    ~LList();
    LList(const LList<T>& rhs);
    LList<T>& operator=(const LList<T>& rhs);
    void clear() { while (!isEmpty()) pop_front(); }
    void push_back(const T& data) { tail->prev = new LListNode<T>(data, tail, tail->prev); tail->prev->prev->next = tail->prev; }
    void push_front(const T& data) { head->next = new LListNode<T>(data, head->next, head);  head->next->next->prev = head->next; }
    T pop_back();
    T pop_front();
    int size();
    bool isEmpty() { return head->next == tail; }
};
template <class T>
LList<T>& LList<T>::operator=(const LList<T>& rhs) {
    if (this == &rhs)
        return *this;
    clear();
    LListNode<T>* ptr = rhs.head->next;
    while (ptr != rhs.tail) {
        push_back(ptr->data);
    }
    return *this;
}
template <class T>
int LList<T>::size() {
    LListNode<T>* ptr = head->next;
    int count = 0;
    while (ptr != tail) {
        count++;
        ptr = ptr->next;
    }
    return count;
}

template <class T>
T LList<T>::pop_back() {
    if (tail->prev == head) //empty list!
        return T(); //returns new object
    T retval = tail->prev->data;
    LListNode<T>* nodeToDelete = tail->prev;
    tail->prev = nodeToDelete->prev;
    nodeToDelete->prev->next = nodeToDelete->next;
    delete nodeToDelete;
    return retval;
}

template <class T>
T LList<T>::pop_front() {
    if (tail->prev == head) //empty list!
        return T(); //returns new object
    T retval = head->next->data;
    LListNode<T>* nodeToDelete = head->next;
    head->next = nodeToDelete->next;
    head->next->prev = head;
    delete nodeToDelete;
    return retval;
}
template <class T>
LList<T>::~LList() {
    clear();
    delete head;
    delete tail;
}
template <class T>
LList<T>::LList(const LList<T>& rhs) {
    head = new LListNode<T>();
    tail = new LListNode<T>();
    head->next = tail;
    tail->prev = head;
    *this = rhs; //call to the assignment operator!!!
}
template <class T>
LList<T>::LList() { //build two nodes, as "Dummies" for teh sake of making things easier
    head = new LListNode<T>();
    tail = new LListNode<T>();
    head->next = tail;
    tail->prev = head;
}

template <class U, class S>
class Pair {
public:
    U first;
    S second;
};

class Employee {
public:
    //constructor without any instantiated vars, hours worked and pay can be initialized to 0
    int id_number;
    string employee_name;
    double hourly_rate;
    double hours_worked;
    double pay;
    };

void open_file(ifstream& input_file) {
    string input_file_path;
    cout << "ENTER FILEPATH:\n";
    cin >> input_file_path;
    input_file.open(input_file_path);
    while (!input_file) {
        cout << "Error: file failed to open";
        cout << "ENTER FILEPATH:\n";
        input_file.clear();
        input_file.open(input_file_path);
    }
}

bool compare_pairs(const Pair<string, int>& i, const Pair<string, int>& j) {
    return (i.second < j.second);
}

int main() {
    ifstream file1, file2;
    int employee_size, id;
    double hours;
    LList<Employee> Employee_linked_list;
    Employee temp;
    Pair<string, int> employee_pay;
    vector< Pair<string, int> > pay_list;

    cout << "Enter Employee Data\n";
    open_file(file1);
    while (file1 >> id) {
        Employee employee;
        employee.id_number = id;
        file1 >> employee.hourly_rate;
        file1.ignore();
        getline(file1, employee.employee_name);
        Employee_linked_list.push_back(employee);
    }
    file1.close();
    employee_size = Employee_linked_list.size();

    cout << "Enter Hours Data\n";
    open_file(file2);
    while (file2 >> id) {
        file2 >> hours;
        for (int i = 0; i < employee_size; i++) {
            temp = Employee_linked_list.pop_front();
            if (id == temp.id_number) {
                temp.hours_worked += hours;
                temp.pay = temp.hourly_rate * temp.hours_worked;
            }
            Employee_linked_list.push_back(temp);
        }
    }
    file2.close();

    cout << "*********Payroll Information********\n";
    while (!Employee_linked_list.isEmpty()) {
        temp = Employee_linked_list.pop_front();
        employee_pay.first = temp.employee_name;
        employee_pay.second = temp.pay;
        pay_list.push_back(employee_pay);
    }
    sort(pay_list.begin(), pay_list.end(), compare_pairs);

    for (int i = (pay_list.size() - 1); i >= 0; i--) {
        cout << pay_list[i].first << ", $" << pay_list[i].second << endl;
    }
    cout << "*********End payroll**************";

    return 0;
}