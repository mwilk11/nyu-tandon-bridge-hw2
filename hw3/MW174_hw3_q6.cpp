#include <iostream>
#include <string>
using namespace std;

int main()
{
    string day_of_week;
    int start_time_hr, start_time_min, call_length;
    char colon;
    double cost_of_call;

    const double WEEKEND_RATE = 0.15;
    const double WEEKDAY_RATE = 0.4;
    const double WEEKNIGHT_RATE = 0.25;

    cout<<"Please input day of the week as one of: Mo, Tu, We, Th, Fr, Sa, Su: ";
    cin>>day_of_week;
    cout<<"Please input start time of call in 24 hour format: ";
    cin>>start_time_hr>>colon>>start_time_min;
    cout<<"Please input the length of the call in minutes: ";
    cin>>call_length;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    if ((day_of_week == "Sa") || (day_of_week == "Su"))
        cost_of_call = call_length * 0.15;
    else
        if ((start_time_hr < 8) || (start_time_hr>18) || ((start_time_hr==18) && (start_time_min>0)))
            cost_of_call = call_length * 0.25;
        else
            cost_of_call = call_length * 0.4;

    cout<<"The cost of the call is $"<<cost_of_call;

    return 0;
}