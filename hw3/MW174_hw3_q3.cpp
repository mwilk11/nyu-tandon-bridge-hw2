#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double a,b,c, x1, x2, discriminant;

    cout<<"Please enter value of a: ";
    cin>>a;
    cout<<"Please enter value of b: ";
    cin>>b;
    cout<<"Please enter value of c: ";
    cin>>c;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(1);

    if ((a == 0) && (b == 0) && (c == 0))
        cout<<"This equation has an infinite number of solutions";

    else if ((a == 0) && (b == 0) && (c != 0))
        cout<<"This equation has no solution";

    else if (a != 0) {
        discriminant = (b * b) - (4 * a * c);
        if (discriminant < 0) {
            cout<<"This equation has no real solution";
        }
        else if (discriminant == 0){
            x1 = ((b * -1) / (2 * a));
            cout<<"This equation has a single real solution x="<<x1;
        }
        else if ((discriminant) > 0){
            x1 = ((b * -1) + (sqrt(discriminant))) / (2 * a);
            x2 = ((b * -1) - (sqrt(discriminant))) / (2 * a);
            cout<<"This equation has a two real solutions x="<<x1<<" or x="<<x2;
        }
    }

    return 0;
}