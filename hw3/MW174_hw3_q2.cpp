#include <iostream>
#include <string>
using namespace std;

int main()
{
    string name, student_status;
    int grad_year, current_year;

    cout<<"Please enter your name: ";
    cin>>name;
    cout<<"Please enter your graduation year: ";
    cin>>grad_year;
    cout<<"Please enter current year: ";
    cin>>current_year;

    if (grad_year <= current_year)
        student_status = "Graduated";
    if (grad_year - current_year == 1)
        student_status = "Senior";
    if (grad_year - current_year == 2)
        student_status = "Junior";
    if (grad_year - current_year == 3)
        student_status = "Sophomore";
    if (grad_year - current_year == 4)
        student_status = "Freshman";
    if (grad_year - current_year > 4)
        student_status = "Not in college yet";

    cout<<name<<", you are a "<<student_status;

    return 0;
}