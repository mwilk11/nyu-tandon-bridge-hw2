#include <iostream>
using namespace std;

int main()
{
    double first_item_price, second_item_price, tax_rate, base_price, price_after_discount, total_price;
    char club_member;

    cout<<"Enter price of first item: ";
    cin>>first_item_price;
    cout<<"Enter price of second item: ";
    cin>>second_item_price;
    cout<<"Does customer have a club card? (Y/N): ";
    cin>>club_member;
    cout<<"Enter tax rate, e.g. 5.5 for 5.5% tax: ";
    cin>>tax_rate;

    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(1);

    base_price = first_item_price + second_item_price;
    cout<<"Base price: "<<base_price<<endl;

    if (first_item_price < second_item_price)
        price_after_discount = (first_item_price * 0.5) + second_item_price;
    else
        price_after_discount = (second_item_price * 0.5) + first_item_price;

    if (club_member == 'y')
        price_after_discount *= 0.9;

    cout<<"Price after discounts: "<<price_after_discount<<endl;

    tax_rate /= 100;
    cout.precision(5);
    total_price = price_after_discount * (1 + tax_rate);
    cout<<"Total price: "<<total_price;

    return 0;
}