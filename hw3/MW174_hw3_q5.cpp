#include <iostream>
#include <string>
using namespace std;

int main()
{
    double weight_lbs, height_in, weight_kg, height_m, bmi;

    const double KGS_PER_POUND = 0.453592;
    const double METERS_PER_INCH = 0.0254;

    string weight_status;

    cout<<"Please enter weight (in pounds): ";
    cin>>weight_lbs;
    cout<<"Please enter height (in inches): ";
    cin>>height_in;

    weight_kg = weight_lbs * KGS_PER_POUND;
    height_m = height_in * METERS_PER_INCH;

    bmi = (weight_kg / (height_m * height_m));

    if (bmi < 18.5)
        weight_status = "Underweight";
    else if ((bmi >= 18.5) && (bmi < 25))
        weight_status = "Normal";
    else if ((bmi >= 25) && (bmi < 30))
        weight_status = "Overweight";
    else if (bmi >= 30)
        weight_status = "Obese";

    cout<<"The weight status is: "<<weight_status;

    return 0;
}