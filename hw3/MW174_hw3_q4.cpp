#include <iostream>
using namespace std;

int main()
{
    double user_number;
    int rounding_method, result;
    const int FLOOR_ROUND = 1;
    const int CEILING_ROUND = 2;
    const int ROUND = 3;

    cout<<"Please enter a Real number: \n";
    cin>>user_number;
    cout<<"Choose your rounding method:\n1. Floor round\n2. Ceiling round\n3. Round to the nearest whole number\n";
    cin>>rounding_method;

    switch (rounding_method) {
        case FLOOR_ROUND:
            result = (int)user_number;
            cout<<result;
            break;
        case CEILING_ROUND:
            result = (int)(user_number + 1);
            cout<<result;
            break;
        case ROUND:
            if (user_number>=(((int)user_number)+0.5))
                result = (int)(user_number + 1);
            else
                result = (int)user_number;
            cout<<result;
            break;
    }

    return 0;
}