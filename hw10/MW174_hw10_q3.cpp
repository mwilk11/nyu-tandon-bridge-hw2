#include <iostream>
#include <vector>
using namespace std;

void resizeArr(int*& arr, int& size);
//resizes an input array to double its size

int main1();
int main2();

int main() {

    main1();
    cout << endl;
    main2();

    return 0;
}

void resizeArr(int*& arr, int& size) {
    int* ptr = new int[(2 * size)];
    for (int i = 0; i < size; i++) {
        ptr[i] = arr[i];
    }
    delete[] arr;
    arr = ptr;
    size = (2 * size);
}

int main1() {

    bool input_flag;
    int input_n;
    int search_n;
    int line_idx = 0;
    int arr_size = 10;
    int* line_arr_ptr = new int[arr_size];
    bool comma_flag;

    cout << "Please enter a sequence of positive integers, each in a separate line.\n";
    cout << "End your input by typing -1.\n";

    input_flag = true;

    while (input_flag) {
        cin >> input_n;
        if (input_n == -1) {
            input_flag = false;
        }
        else {
            line_arr_ptr[line_idx] = input_n;
            line_idx++;
            if (line_idx == arr_size) {
                resizeArr(line_arr_ptr, arr_size);
            }
        }
    }

    cout << "Please enter a number you want to search.\n";
    cin >> search_n;
    cout << search_n << " shows in lines ";
    comma_flag = false;

    for (int i = 0; i <= line_idx; i++) {
        if (search_n == line_arr_ptr[i]) {
            if (comma_flag) {
                cout <<", ";
            }
            cout << (i + 1);
            comma_flag = true;
        }
    }
    cout << ".";

    return 0;

}

int main2() {

    bool input_flag;
    int input_n;
    int search_n;
    vector<int> line_idx_arr;
    bool comma_flag;

    cout << "Please enter a sequence of positive integers, each in a separate line.\n";
    cout << "End your input by typing -1.\n";

    input_flag = true;

    while (input_flag) {
        cin >> input_n;
        if (input_n == -1) {
            input_flag = false;
        }
        else {
            line_idx_arr.push_back(input_n);
        }
    }

    cout << "Please enter a number you want to search.\n";
    cin >> search_n;
    cout << search_n << " shows in lines ";
    comma_flag = false;

    for (int i = 0; i < line_idx_arr.size(); i++) {
        if (search_n == line_idx_arr[i]) {
            if (comma_flag) {
                cout <<", ";
            }
            cout << (i + 1);
            comma_flag = true;
        }
    }
    cout << ".";

    return 0;
}