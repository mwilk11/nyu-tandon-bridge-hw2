#include <iostream>
#include <string>
#include <vector>
using namespace std;

string* createWordsArray(string sentence, int& outWordsArrSize);
//input sentence string and variable for the output word array size
//returns a pointer to an array of strings that contains all the words in the input sentence
//updates the output parameter, outWordsArrSize, with the logical size of the word array (word count)

int main() {

    string sentence;
    int wordCount;
    string* word_pointer;

    cout << "Please enter a sentence:\n";
    getline(cin, sentence);
    word_pointer = createWordsArray(sentence, wordCount);

    cout << "Word count is: " << wordCount << endl;

    for (int i = 0; i < wordCount; i++) {
        cout << word_pointer[i] << endl;
    }

    return 0;
}

string* createWordsArray(string sentence, int& outWordsArrSize) {
    string word;
    vector<string> word_array;
    string* string_pointer;

    for (int i = 0; i < sentence.length(); i++) {
        if (sentence[i] == ' ') {
            word_array.push_back(word);
            word = "";
        }
        else {
            word += sentence[i];
        }
    }
    word_array.push_back(word);
    outWordsArrSize = word_array.size();
    string_pointer = &word_array[0];
    return string_pointer;
}
