#include <iostream>
#include <vector>
using namespace std;

int* findMissing(int arr[], int n, int& resArrSize);
//takes an array of integers arr and its logical size n
//all elements in arr are in the range {0, 1, 2, ... , n}
//returns a pointer to an array containing all the numbers missing from the input array
//updates output parameter, resArrSize, with the logical size of the new array that was created.

int main() {

    int arr[6] = {3, 1, 3, 0, 6, 4};
    int arr_size = 6;
    int new_arr_size;
    int* new_arr_pointer;

    new_arr_pointer = findMissing(arr, arr_size, new_arr_size);

    cout << "There are " << new_arr_size << " missing values in the input array" << endl;
    cout << "The values are: ";
    for (int i = 0; i < new_arr_size; i++) {
        cout << new_arr_pointer[i] << " ";
    }

    return 0;
}

int* findMissing(int arr[], int n, int& resArrSize) {
    int n_arr[n+1];
    vector<int> missing_vals;
    int* missingValArrPointer;

    for (int i = 0; i <= n; i++) {
        n_arr[i] = i;
    }
    for (int j = 0; j < n; j++) {
        n_arr[arr[j]] = -1;
    }
    for (int k = 0; k < n; k++) {
        if (n_arr[k] != -1) {
            missing_vals.push_back(n_arr[k]);
        }
    }
    resArrSize = missing_vals.size();
    missingValArrPointer = &missing_vals[0];
    return missingValArrPointer;
}