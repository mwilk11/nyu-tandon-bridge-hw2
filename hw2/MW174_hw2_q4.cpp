#include <iostream>
using namespace std;

int main()
{
    int input_1, input_2;

    cout<<"Please enter two positive integers, separated by a space:\n";
    cin>>input_1>>input_2;

    cout<<input_1<<" + "<<input_2<<" = "<<input_1+input_2<<endl;
    cout<<input_1<<" - "<<input_2<<" = "<<input_1-input_2<<endl;
    cout<<input_1<<" * "<<input_2<<" = "<<input_1*input_2<<endl;
    cout<<input_1<<" / "<<input_2<<" = "<<(double)input_1/(double)input_2<<endl;
    cout<<input_1<<" div "<<input_2<<" = "<<input_1/input_2<<endl;
    cout<<input_1<<" mod "<<input_2<<" = "<<input_1%input_2;

    return 0;
}