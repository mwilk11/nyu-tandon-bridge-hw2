#include <iostream>
using namespace std;

int main()
{
    int quarters, dimes, nickels, pennies, dollars;
    double cents, total;

    const double QUARTER_VAL = 0.25, DIME_VAL = 0.1, NICKEL_VAL = 0.05, PENNY_VAL = 0.01;
    const int CENTS_PER_DOLLAR = 100;

    cout<<"Please enter your amount in the format of dollars and cents separated by a space:\n";
    cin>>dollars>>cents;

    total = dollars + cents/CENTS_PER_DOLLAR;

    quarters = (total/QUARTER_VAL);
    total -= quarters * QUARTER_VAL;
    dimes = (total/DIME_VAL);
    total -= dimes * DIME_VAL;
    nickels = (total/NICKEL_VAL);
    total -= nickels * NICKEL_VAL;
    pennies = (total/PENNY_VAL);
    total -= pennies * PENNY_VAL;

    cout<<dollars<<" dollars and "<<cents<<" cents are:\n";
    cout<<quarters<<" quarters, "<<dimes<<" dimes, "<<nickels<<" nickels and "<<pennies<<" pennies";

    return 0;
}