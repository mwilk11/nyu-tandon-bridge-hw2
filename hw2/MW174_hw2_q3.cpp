#include <iostream>
using namespace std;

int main()
{
    int days_john, hours_john, minutes_john, days_bill, hours_bill, minutes_bill;
    int combined_days, combined_hours, combined_minutes;
    int total_days, total_hours, total_minutes;

    const int MIN_PER_HOUR = 60, HOURS_PER_DAY = 24;

    cout<<"Please enter the number of days John has worked: ";
    cin>>days_john;
    cout<<"Please enter the number of hours John has worked: ";
    cin>>hours_john;
    cout<<"Please enter the number of minutes John has worked: ";
    cin>>minutes_john;

    cout<<"\nPlease enter the number of days Bill has worked: ";
    cin>>days_bill;
    cout<<"Please enter the number of hours Bill has worked: ";
    cin>>hours_bill;
    cout<<"Please enter the number of minutes Bill has worked: ";
    cin>>minutes_bill;

    combined_days = days_john + days_bill;
    combined_hours = hours_john + hours_bill;
    combined_minutes = minutes_john + minutes_bill;

    total_minutes = combined_minutes - ((combined_minutes / MIN_PER_HOUR) * MIN_PER_HOUR);
    total_hours = combined_hours + (combined_minutes / MIN_PER_HOUR);
    total_days = combined_days + (total_hours / HOURS_PER_DAY);
    total_hours -= (total_hours / HOURS_PER_DAY) * HOURS_PER_DAY;

    cout<<"\nThe total time both of them worked together is: "<<total_days<<" days, "<<total_hours<<" hours and "<<total_minutes<<" minutes.";

    return 0;
}