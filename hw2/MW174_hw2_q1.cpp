#include <iostream>
using namespace std;

int main()
{
    int quarters, dimes, nickels, pennies, dollars, cents;
    double total;

    const double QUARTER_VAL = 0.25, DIME_VAL = 0.1, NICKEL_VAL = 0.05, PENNY_VAL = 0.01;
    const int CENTS_PER_DOLLAR = 100;

    cout<<"Please enter number of coins:\n";
    cout<<"# of quarters: ";
    cin>>quarters;
    total = quarters * QUARTER_VAL;
    cout<<"# of dimes: ";
    cin>>dimes;
    total += dimes * DIME_VAL;
    cout<<"# of nickels: ";
    cin>>nickels;
    total += nickels * NICKEL_VAL;
    cout<<"# of pennies: ";
    cin>>pennies;
    total += pennies * PENNY_VAL;

    dollars = (int)total;
    cents = (total - dollars) * CENTS_PER_DOLLAR;

    cout<<"The total is "<<dollars<<" dollars and "<<cents<<" cents";

    return 0;
}