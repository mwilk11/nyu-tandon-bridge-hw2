
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
using namespace std;

int main() {
    char world[20][20];
    int move_idx_arr[4] = {0, 0, 0, 0};
    int moves;
    int rand_move;
    int rows = 20, cols = 20;
    int ant_counter = 0;
    int doodlebug_counter = 0;
    int x, y;
    vector<int>ant_idx1;
    vector<int>ant_idx2;
    vector<int>dbug_idx1;
    vector<int>dbug_idx2;
    int time_step = 2;
    char char_input;
    int dbugs;
    int ants;

    srand(time(0));

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            world[i][j] = '-';
        }
    }

    for (int m = 0; m < 100; m++) {
        x = (rand() % 21);
        y = (rand() % 21);
        if (world[x][y] != 'o') {
            world[x][y] = 'o';
            ant_idx1.push_back(x);
            ant_idx2.push_back(y);
            ant_counter += 1;
        }
        else {
            m -= 1;
        }
    }

    for (int q = 0; q < 5; q++) {
        x = (rand() % 21);
        y = (rand() % 21);
        if ((world[x][y] != 'o') || (world[x][y] != 'X')) {
            world[x][y] = 'X';
            dbug_idx1.push_back(x);
            dbug_idx2.push_back(y);
            doodlebug_counter += 1;
        }
        else {
            q -= 1;
        }
    }

    cout << "doodlebugs: " << doodlebug_counter << endl;
    cout << "ants: " << ant_counter << endl;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << world[i][j] << ' ';
        }
        cout << endl;
    }

    while(time_step < 100) {
        cout << "Please enter + sign to move to the next time step";
        cin >> char_input;
        cout << endl << "This is time step " << time_step << endl;

        dbugs = doodlebug_counter;
        for (int h = 0; h < dbugs; h++) {
            moves = 0;
            if ((dbug_idx2[h] != 0) && ((world[dbug_idx1[h]][(dbug_idx2[h] - 1)] == 'o') ||
                                        (world[dbug_idx1[h]][(dbug_idx2[h] - 1)] == '-'))) {
                move_idx_arr[moves] = 1;
                moves += 1;
            }
            if ((dbug_idx2[h] != 19) && ((world[dbug_idx1[h]][(dbug_idx2[h] + 1)] == 'o') ||
                                         (world[dbug_idx1[h]][(dbug_idx2[h] + 1)] == '-'))) {
                move_idx_arr[moves] = 2;
                moves += 1;
            }
            if ((dbug_idx1[h] != 19) && ((world[(dbug_idx1[h] + 1)][dbug_idx2[h]] == 'o') ||
                                         (world[(dbug_idx1[h] + 1)][dbug_idx2[h]] == '-'))) {
                move_idx_arr[moves] = 3;
                moves += 1;
            }
            if ((dbug_idx1[h] != 0) && ((world[(dbug_idx1[h] - 1)][dbug_idx2[h]] == 'o') ||
                                        (world[(dbug_idx1[h] - 1)][dbug_idx2[h]] == '-'))) {
                move_idx_arr[moves] = 4;
                moves += 1;
            }

            if (moves != 0) {
                rand_move = move_idx_arr[(rand() % moves)];
                if (rand_move == 1) {
                    if (world[dbug_idx1[h]][(dbug_idx2[h] - 1)] == 'o') {
                        ant_counter -= 1;
                    }
                    world[dbug_idx1[h]][(dbug_idx2[h] - 1)] = 'X';
                    world[dbug_idx1[h]][dbug_idx2[h]] = '-';
                    dbug_idx2[h] -= 1;
                }
                if (rand_move == 2) {
                    if (world[dbug_idx1[h]][(dbug_idx2[h] + 1)] == 'o') {
                        ant_counter -= 1;
                    }
                    world[dbug_idx1[h]][(dbug_idx2[h] + 1)] = 'X';
                    world[dbug_idx1[h]][dbug_idx2[h]] = '-';
                    dbug_idx2[h] += 1;
                }
                if (rand_move == 3) {
                    if (world[(dbug_idx1[h] + 1)][dbug_idx2[h]] == 'o') {
                        ant_counter -= 1;
                    }
                    world[(dbug_idx1[h] + 1)][dbug_idx2[h]] = 'X';
                    world[dbug_idx1[h]][dbug_idx2[h]] = '-';
                    dbug_idx1[h] += 1;
                }
                if (rand_move == 4) {
                    if (world[(dbug_idx1[h] - 1)][dbug_idx2[h]] == 'o') {
                        ant_counter -= 1;
                    }
                    world[(dbug_idx1[h] - 1)][dbug_idx2[h]] = 'X';
                    world[dbug_idx1[h]][dbug_idx2[h]] = '-';
                    dbug_idx1[h] -= 1;
                }
            }
        }

        if ((time_step % 8) == 0) {
            dbugs = doodlebug_counter;
            for (int h = 0; h < dbugs; h++) {
                moves = 0;
                if ((dbug_idx2[h] != 0) && (world[dbug_idx1[h]][(dbug_idx2[h] - 1)] == '-')) {
                    move_idx_arr[moves] = 1;
                    moves += 1;
                }
                if ((dbug_idx2[h] != 19) && (world[dbug_idx1[h]][(dbug_idx2[h] + 1)] == '-')) {
                    move_idx_arr[moves] = 2;
                    moves += 1;
                }
                if ((dbug_idx1[h] != 19) && (world[(dbug_idx1[h] + 1)][dbug_idx2[h]] == '-')) {
                    move_idx_arr[moves] = 3;
                    moves += 1;
                }
                if ((dbug_idx1[h] != 0) && (world[(dbug_idx1[h] - 1)][dbug_idx2[h]] == '-')) {
                    move_idx_arr[moves] = 4;
                    moves += 1;
                }

                if (moves != 0) {
                    rand_move = move_idx_arr[(rand() % moves)];
                    if (rand_move == 1) {
                        world[dbug_idx1[h]][(dbug_idx2[h] - 1)] = 'X';
                        dbug_idx1.push_back(dbug_idx1[h]);
                        dbug_idx2.push_back(dbug_idx2[h] - 1);
                        doodlebug_counter += 1;
                    }
                    if (rand_move == 2) {
                        world[dbug_idx1[h]][(dbug_idx2[h] + 1)] = 'X';
                        dbug_idx1.push_back(dbug_idx1[h]);
                        dbug_idx2.push_back(dbug_idx2[h] + 1);
                        doodlebug_counter += 1;
                    }
                    if (rand_move == 3) {
                        world[(dbug_idx1[h] + 1)][dbug_idx2[h]] = 'X';
                        dbug_idx1.push_back(dbug_idx1[h] + 1);
                        dbug_idx2.push_back(dbug_idx2[h]);
                        doodlebug_counter += 1;
                    }
                    if (rand_move == 4) {
                        world[(dbug_idx1[h] - 1)][dbug_idx2[h]] = 'X';
                        dbug_idx1.push_back(dbug_idx1[h] - 1);
                        dbug_idx2.push_back(dbug_idx2[h]);
                        doodlebug_counter += 1;
                    }
                }
            }
        }


        if ((time_step % 3) == 0) {
            ants = ant_counter;
            for (int a = 0; a < ants; a++) {
                moves = 0;
                if ((ant_idx2[a] != 0) && (world[ant_idx1[a]][(ant_idx2[a] - 1)] == '-')) {
                    move_idx_arr[moves] = 1;
                    moves += 1;
                }
                if ((ant_idx2[a] != 20) && (world[ant_idx1[a]][(ant_idx2[a] + 1)] == '-')) {
                    move_idx_arr[moves] = 2;
                    moves += 1;
                }
                if ((ant_idx1[a] != 20) && (world[(ant_idx1[a] + 1)][ant_idx2[a]] == '-')) {
                    move_idx_arr[moves] = 3;
                    moves += 1;
                }
                if ((ant_idx1[a] != 0) && (world[(ant_idx1[a] - 1)][ant_idx2[a]] == '-')) {
                    move_idx_arr[moves] = 4;
                    moves += 1;
                }

                if (moves != 0) {
                    rand_move = move_idx_arr[(rand() % moves)];
                    if (rand_move == 1) {
                        world[ant_idx1[a]][(ant_idx2[a] - 1)] = 'o';
                        ant_idx1.push_back(ant_idx1[a]);
                        ant_idx2.push_back(ant_idx2[a] - 1);
                        ant_counter += 1;
                    }
                    if (rand_move == 2) {
                        world[ant_idx1[a]][(ant_idx2[a] + 1)] = 'o';
                        ant_idx1.push_back(ant_idx1[a]);
                        ant_idx2.push_back(ant_idx2[a] + 1);
                        ant_counter += 1;
                    }
                    if (rand_move == 3) {
                        world[(ant_idx1[a] + 1)][ant_idx2[a]] = 'o';
                        ant_idx1.push_back(ant_idx1[a] + 1);
                        ant_idx2.push_back(ant_idx2[a]);
                        ant_counter += 1;
                    }
                    if (rand_move == 4) {
                        world[(ant_idx1[a] - 1)][ant_idx2[a]] = 'o';
                        ant_idx1.push_back(ant_idx1[a] - 1);
                        ant_idx2.push_back(ant_idx2[a]);
                        ant_counter += 1;
                    }
                }
            }
        }

        ants = ant_counter;
        for (int a = 0; a < ants; a++) {
            moves = 0;

            if ((ant_idx2[a] != 0) && (world[ant_idx1[a]][(ant_idx2[a] - 1)] == '-') && (world[ant_idx1[a]][(ant_idx2[a] - 1)] != 'X')) {
                move_idx_arr[moves] = 1;
                moves += 1;
            }

            if ((ant_idx2[a] != 19) && (world[ant_idx1[a]][(ant_idx2[a] + 1)] == '-') && (world[ant_idx1[a]][(ant_idx2[a] + 1)]  != 'X')) {
                move_idx_arr[moves] = 2;
                moves += 1;
            }

            if ((ant_idx1[a] != 19) && (world[(ant_idx1[a] + 1)][ant_idx2[a]] == '-') && (world[(ant_idx1[a] + 1)][ant_idx2[a]] != 'X')) {
                move_idx_arr[moves] = 3;
                moves += 1;
            }

            if ((ant_idx1[a] != 0) && (world[(ant_idx1[a] - 1)][ant_idx2[a]] == '-') && (world[(ant_idx1[a] - 1)][ant_idx2[a]] != 'X')) {
                move_idx_arr[moves] = 4;
                moves += 1;
            }

            if (moves != 0) {
                rand_move = move_idx_arr[(rand() % moves)];
                if (rand_move == 1) {
                    world[ant_idx1[a]][ant_idx2[a]] = '-';
                    world[ant_idx1[a]][(ant_idx2[a] - 1)] = 'o';
                    ant_idx2[a] -= 1;
                }
                if (rand_move == 2) {
                    world[ant_idx1[a]][ant_idx2[a]] = '-';
                    world[ant_idx1[a]][(ant_idx2[a] + 1)] = 'o';
                    ant_idx2[a] += 1;
                }
                if (rand_move == 3) {
                    world[ant_idx1[a]][ant_idx2[a]] = '-';
                    world[(ant_idx1[a] + 1)][ant_idx2[a]] = 'o';
                    ant_idx1[a] += 1;
                }
                if (rand_move == 4) {
                    world[ant_idx1[a]][ant_idx2[a]] = '-';
                    world[(ant_idx1[a] - 1)][ant_idx2[a]] = 'o';
                    ant_idx1[a] -= 1;
                }
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                cout << world[i][j] << ' ';
            }
            cout << endl;
        }

        time_step += 1;
    }

    return 0;
}

